* Running the project

- rails db:migrate
- rails db:seed (to create initial record)
- rails s


* Models created

- Guest
- Contact (for accommodating multiple guest contacts)
- Reservation


* Accessing records through rails console

- Guest has_many -> Contact (Guest.first.contacts)
- Guest has_many -> Reservation (Guest.first.reservations)

* Endpoint -> localhost:3000/api/v1/integration/make_reservation

* Included postman request as well that containts both sample payloads