class Api::V1::IntegrationController < ApplicationController
    skip_before_action :verify_authenticity_token

    def make_reservation
        if params.has_key?(:reservation)
            guest = Integration::WithReservation.new params[:reservation]
            render json: :saved
        elsif params.has_key?(:guest)
            guest = Integration::WithGuest.new params
            render json: :saved
        end
    end
end