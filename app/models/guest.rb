class Guest < ApplicationRecord

    has_many :contacts
    has_many :reservations
end
