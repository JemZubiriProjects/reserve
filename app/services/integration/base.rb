module Integration
    class Base

        def start_integration_engine
            #Save guests
            guest = Guest.find_by_id @guest_data[:id]
            if guest.present? 
                guest.update @guest_data

                save_contacts guest
                save_reservation guest
            else
                new_guest = Guest.create(@guest_data)
                if new_guest.save!
                    save_contacts new_guest
                    save_reservation new_guest
                end
            end
            
        end

        def save_contacts guest
            @guest_contacts.each do |contact|
                if Contact.find_by_phone_number(contact).nil?
                    guest.contacts.create(phone_number: contact)
                end 
            end
        end

        def save_reservation guest
            guest.reservations.create @reservation_details
        end
    end
end