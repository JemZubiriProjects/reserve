module Integration
    class WithGuest < Integration::Base
        
        attr_accessor :guest_data, :guest_contacts, :reservation_details

        def initialize params
            # Parse payload
            @guest_data = {
                id: params[:guest][:id],
                first_name: params[:guest][:first_name],
                last_name: params[:guest][:last_name],
                email: params[:guest][:email]
            }
            @guest_contacts = [params[:guest][:phone]]
            @reservation_details = {
                start_date: params[:start_date],
                end_date: params[:end_date],
                night: params[:nights],
                adult_guests: params[:adults],
                child_guests: params[:children],
                infant_guests: params[:infants],
                total_guests: params[:guests],
                localized_guests: params[:guests].to_s + " guests",
                currency: params[:currency],
                payout_price: params[:payout_price],
                security_price: params[:security_price],
                total_price: params[:total_price],
                status: params[:status]
            }

            start_integration_engine
        end
    end
end

