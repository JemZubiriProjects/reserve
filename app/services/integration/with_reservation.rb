module Integration
    class WithReservation < Integration::Base

        attr_accessor :guest_data, :guest_contacts, :reservation_details

        def initialize params
            # Parse payload
            @guest_data = {
                id: params[:guest_id],
                first_name: params[:guest_first_name],
                last_name: params[:guest_last_name],
                email: params[:guest_email]
            }
            @guest_contacts = params[:guest_phone_numbers]
            @reservation_details = {
                start_date: params[:start_date],
                end_date: params[:end_date],
                night: params[:nights],
                adult_guests: params[:guest_details][:number_of_adults],
                child_guests: params[:guest_details][:number_of_children],
                infant_guests: params[:guest_details][:number_of_infants],
                total_guests: params[:guest_details][:number_of_adults].to_i + params[:guest_details][:number_of_children].to_i + params[:guest_details][:number_of_infants].to_i,
                localized_guests: params[:guest_details][:localized_description],
                currency: params[:host_currency],
                payout_price: params[:expected_payout_amount],
                security_price: params[:listing_security_price_accurate],
                total_price: params[:total_paid_amount_accurate],
                status: params[:status_type]
            }

            start_integration_engine
        end
    end
end


# {
#     "reservation": {
#         "start_date": "2020-03-12",
#         "end_date": "2020-03-16",
#         "expected_payout_amount": "3800.00",
#         "guest_details": {
#             "localized_description": "4 guests",
#             "number_of_adults": 2,
#             "number_of_children": 2,
#             "number_of_infants": 0
#         },
#         "guest_email": "wayne_woodbridge@bnb.com",
#         "guest_first_name": "Wayne",
#         "guest_id": 1,
#         "guest_last_name": "Woodbridge",
#         "guest_phone_numbers": [
#             "639123456789",
#             "639123456789"
#         ],
#         "listing_security_price_accurate": "500.00",
#         "host_currency": "AUD",
#         "nights": 4,
#         "number_of_guests": 4,
#         "status_type": "accepted",
#         "total_paid_amount_accurate": "4500.00"
#     }
# }