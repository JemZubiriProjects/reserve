Rails.application.routes.draw do
  
  
  namespace :api do
    namespace :v1 do
      resources :integration do
        collection do 
          post 'make_reservation'
        end
      end
    end
  end
  

end
