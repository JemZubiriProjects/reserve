class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.references :guest, null: false, foreign_key: true
      t.string :phone_number

      t.timestamps
    end
  end
end
