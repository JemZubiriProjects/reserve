class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.references :guest, null: false, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.integer :night
      t.integer :adult_guests
      t.integer :child_guests
      t.integer :infant_guests
      t.integer :total_guests
      t.string :localized_guests
      t.string :currency
      t.string :payout_price
      t.string :security_price
      t.string :total_price
      t.boolean :status

      t.timestamps
    end
  end
end
