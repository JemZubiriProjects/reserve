guest_phone_numbers = ["639123456789","639123456788"],
guest = Guest.create(
    first_name: "Wayne",
    last_name: "Woodbridge",
    email: "wayne_woodbridge@bnb.com"
)

guest_phone_numbers.each do |contact|
    guest.contacts.create(phone_number: contact)
end

guest.reservations.create(
    start_date: "2020-03-12",
    end_date: "2020-03-16",
    night: 4,
    adult_guests: 2,
    child_guests: 2,
    infant_guests: 0,
    total_guests: 4,
    localized_guests: "4 guests",
    currency: "AUD",
    payout_price: "3800",
    security_price: "500",
    total_price: "4500",
    status: "accepted"
)